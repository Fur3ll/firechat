package com.fur3ll.chat.fabric;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.fur3ll.chat.helper.NotificationHelper;
import com.fur3ll.chat.ui.fragments.chat.viewmodel.ChatFragmentViewModel;
import com.fur3ll.chat.ui.fragments.list.viewmodel.FriendListViewModel;
import com.google.firebase.firestore.FirebaseFirestore;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private FirebaseFirestore firebaseFirestore;
    private NotificationHelper notificationHelper;
    private String friendDocumentId;
    private String userDocumentId;

    public ViewModelFactory(FirebaseFirestore firebaseFirestore, NotificationHelper notificationHelper) {
        this.firebaseFirestore = firebaseFirestore;
        this.notificationHelper = notificationHelper;
    }

    public void setFriendDocumentId(String friendDocumentId) {
        this.friendDocumentId = friendDocumentId;
    }

    public void setUserDocumentId(String userDocumentId) {
        this.userDocumentId = userDocumentId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(FriendListViewModel.class)) {
            return (T) new FriendListViewModel(firebaseFirestore, notificationHelper);
        }
        if (modelClass.isAssignableFrom(ChatFragmentViewModel.class)) {
            return (T) new ChatFragmentViewModel(firebaseFirestore, friendDocumentId, userDocumentId);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
