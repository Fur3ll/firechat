package com.fur3ll.chat.helper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.service.notification.StatusBarNotification;

import androidx.core.app.NotificationCompat;

import com.fur3ll.chat.R;
import com.fur3ll.chat.ui.activities.splash.SplashActivity;


public class NotificationHelper {

    private final static String NOTIFICATION_CHANNEL_DEFAULT = "Default";
    private final static String NOTIFICATION_CHANNEL_SERVICE = "Service";
    private final static String NOTIFICATION_SERVICE_TEXT = "Service for firestore notifications";
    private Context context;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    private int id;

    public NotificationHelper(Context context, NotificationManager notificationManager) {
        this.context = context;
        this.notificationManager = notificationManager;
        id = context.getResources().getInteger(R.integer.notification_init_index);
    }

    public void createNotification(String title, String message, String channelId, String groupName) {

        //create action on notification tap
        Intent resultIntent = new Intent(context, SplashActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context,
                0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (channelId == null) {
            channelId = NOTIFICATION_CHANNEL_DEFAULT;
        }

        if (groupName == null) {
            groupName = NOTIFICATION_CHANNEL_DEFAULT;
        }

        //check if group in active notifications exist, if not remove from set
        if (!isGroupExist(groupName)) {
            //create group
            notificationManager.notify(getNextId(notificationManager.getActiveNotifications()), createGroupNotification(channelId, title, groupName, resultPendingIntent).build());
            //create notification first in group
            builder = createNotification(channelId, message, title, groupName, resultPendingIntent);
        } else {
            //group exist so just create new notification
            builder = createNotification(channelId, message, title, groupName, resultPendingIntent);
        }

        notificationManager.notify(getNextId(notificationManager.getActiveNotifications()), builder.build());
    }

    private NotificationCompat.Builder createGroupNotification(String channelId, String title, String groupName, PendingIntent intent) {

        builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(title)
                .setGroup(groupName)
                .setStyle(new NotificationCompat.InboxStyle())
                .setGroupSummary(true)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(intent);

        return builder;
    }

    private NotificationCompat.Builder createNotification(String channelId, String message, String title, String groupName, PendingIntent intent) {

        builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(title)
                .setGroup(groupName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(intent);

        return builder;
    }

    private int getNextId(StatusBarNotification[] activeNotifications) {

        int i = id;
        for (StatusBarNotification activeNotification : activeNotifications) {
            if (activeNotification.getId() > i) {
                i = activeNotification.getId();
            }
        }
        return ++i;
    }

    private boolean isGroupExist(String groupName) {

        boolean groupExist = false;

        StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();

        for (StatusBarNotification activeNotification : activeNotifications) {

            String notificationGroup = activeNotification.getNotification().getGroup();

            if (notificationGroup != null && !notificationGroup.equals(groupName)) {
                if (groupExist) {
                    break;
                }
            } else if (notificationGroup != null) {
                groupExist = true;
            }
        }

        return groupExist;
    }

    public Notification getServiceNotification(NotificationCompat.Action action) {

        createNotificationChannel(NOTIFICATION_CHANNEL_SERVICE,
                NOTIFICATION_CHANNEL_SERVICE,
                NOTIFICATION_CHANNEL_SERVICE,
                NotificationCompat.PRIORITY_DEFAULT);

        builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_SERVICE)
                .setSmallIcon(R.drawable.ic_close_black_24dp)
                .setContentTitle(NOTIFICATION_CHANNEL_SERVICE)
                .setGroup(NOTIFICATION_CHANNEL_SERVICE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentText(NOTIFICATION_SERVICE_TEXT)
                .setAutoCancel(false)
                .addAction(action);

        return builder.build();
    }

    public void createNotificationChannel(String identifier, String name) {

        String channelDescription = context.getResources().getString(R.string.channel_description);

        if (identifier == null || name == null) {
            identifier = NOTIFICATION_CHANNEL_DEFAULT;
            name = NOTIFICATION_CHANNEL_DEFAULT;
        }

        createNotificationChannel(identifier, name, channelDescription, NotificationManager.IMPORTANCE_LOW);
    }

    private void createNotificationChannel(String identifier, String name, String description, int importance) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return;
        }

        NotificationChannel channel = new NotificationChannel(identifier, name, importance);

        if (description != null && !description.isEmpty()) {
            channel.setDescription(description);
        }

        notificationManager.createNotificationChannel(channel);
    }
}
