package com.fur3ll.chat.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.fur3ll.chat.mapper.BaseMapper;
import com.fur3ll.chat.mapper.FriendMapper;
import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.FriendModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Map;

public class CreateUserService extends IntentService implements DatabaseNaming.CollectionUsers {

    public final static String ON_FAILURE = "onFailure: ";
    public final static String INTENT_EXTRA = "token";
    public final static String CREATE_USER = "create_user";
    public final static String TAG = "CreateUserService";
    private FirebaseUser firebaseUser;
    private CollectionReference usersCollection;

    public CreateUserService() {
        super(CREATE_USER);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        usersCollection = FirebaseFirestore.getInstance().collection(COLLECTION_USERS);

        String userToken = intent.getStringExtra(INTENT_EXTRA);
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();

        if (userToken != null || email != null) {

            usersCollection
                    .whereEqualTo(FIELD_EMAIL, email)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            if (task.getResult().size() == 1) {
                                updateUser(task.getResult().getDocuments().get(0).getId(), userToken);
                            } else if (task.getResult().isEmpty()) {
                                BaseMapper<FriendModel, Map<String, String>> mapper = new FriendMapper();
                                Map<String, String> userInfo = mapper.transformFrom(initUser(userToken));
                                insertUser(userInfo);
                            }
                        }
                    });
        }
    }

    private void insertUser(Map<String, String> userInfo) {
        usersCollection
                .document()
                .set(userInfo)
                .addOnFailureListener(e -> Log.e(TAG, ON_FAILURE + e.toString()));
    }

    private void updateUser(String documentId, String userToken) {
        usersCollection
                .document(documentId)
                .update(FIELD_TOKEN, userToken)
                .addOnFailureListener(e -> Log.e(TAG, ON_FAILURE + e.toString()));
    }

    private FriendModel initUser(String token) {
        FriendModel user = new FriendModel();
        user.setName(firebaseUser.getDisplayName());
        user.setAvatar(firebaseUser.getPhotoUrl().toString());
        user.setEmail(firebaseUser.getEmail());
        user.setToken(token);
        return user;
    }
}
