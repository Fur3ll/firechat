package com.fur3ll.chat.services;

import android.content.Intent;

import androidx.annotation.NonNull;

import com.fur3ll.chat.App;
import com.fur3ll.chat.helper.NotificationHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MessageReceiverService extends FirebaseMessagingService {

    /**
     * Called if InstanceID token is updated. Update user token in firestore
     */
    @Override
    public void onNewToken(@NonNull String token) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(),CreateUserService.class);
            intent.putExtra(CreateUserService.INTENT_EXTRA,token);
            startService(intent);
        }
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        
        RemoteMessage.Notification notification = remoteMessage.getNotification();

        NotificationHelper notificationHelper = ((App)getApplication()).getNotificationHelper();

        if (notification != null ) {
            if (notification.getChannelId() == null) {
                notificationHelper.createNotificationChannel(notification.getChannelId(), notification.getChannelId());
            }

            notificationHelper.createNotification(notification.getTitle(),
                    notification.getBody(),
                    notification.getChannelId(),
                    notification.getChannelId());
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }
}

