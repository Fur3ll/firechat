package com.fur3ll.chat.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.fur3ll.chat.App;
import com.fur3ll.chat.R;
import com.fur3ll.chat.helper.NotificationHelper;
import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.FriendModel;
import com.fur3ll.chat.models.MessageModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageListenerService extends Service implements
        DatabaseNaming.CollectionConversation,
        DatabaseNaming.CollectionConversation.CollectionMessages, DatabaseNaming.CollectionUsers {

    public static final String TAG = "MessageListenerService";
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_SERVICE";
    public static final String NOTIFICATION_STOP_SERVICE_TITLE = "Stop service";
    private List<ListenerRegistration> listenersList = new ArrayList<>();
    private NotificationHelper notificationHelper;
    private FirebaseFirestore firestore;
    private Map<String, String> friendsDocumentIds = new HashMap<>();

    private boolean initialisation = false;
    private int size = 0;

    public MessageListenerService() {
    }

    @Override
    public void onCreate() {
        notificationHelper = ((App) getApplication()).getNotificationHelper();
        firestore = FirebaseFirestore.getInstance();
        initListeners();
        super.onCreate();
    }

    private void startForegroundService() {
        startForeground(1, createServiceNotification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            String action = intent.getAction();

            assert action != null;
            switch (action) {
                case ACTION_START_FOREGROUND_SERVICE:
                    startForegroundService();
                    break;
                case ACTION_STOP_FOREGROUND_SERVICE:
                    stopForegroundService();
                    break;
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private Notification createServiceNotification() {

        //Cancel button
        Intent startIntent = new Intent(this, MessageListenerService.class);
        startIntent.setAction(ACTION_STOP_FOREGROUND_SERVICE);

        PendingIntent pendingStart = PendingIntent.getService(this, 0,
                startIntent, 0);

        NotificationCompat.Action startAction = new NotificationCompat.
                Action(R.drawable.ic_close_black_24dp,
                NOTIFICATION_STOP_SERVICE_TITLE, pendingStart);

        return notificationHelper.getServiceNotification(startAction);
    }

    private void stopForegroundService() {
        if (!listenersList.isEmpty()) {
            for (ListenerRegistration listener : listenersList) {
                listener.remove();
            }
        }
        stopForeground(true);
        stopSelf();
    }

    private void setInitialisation() {
        if (size == 0) {
            initialisation = true;
        } else {
            size--;
        }
    }

    private void initListeners() {

        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();

        firestore.collection(COLLECTION_USERS)
                .get().addOnSuccessListener(snapshots -> {
            if (snapshots.isEmpty()) {
                return;
            }

            for (DocumentSnapshot itm : snapshots.getDocuments()) {
                FriendModel friendModel = itm.toObject(FriendModel.class);
                if (!friendModel.getEmail().equals(email)) {
                    friendsDocumentIds.put(friendModel.getName(), itm.getId());
                } else {
                    initChatsMessagesListeners(itm);
                }
            }
        });
    }

    private void initChatsMessagesListeners(DocumentSnapshot itm) {
        firestore.collection(COLLECTION_CONVERSATION).
                whereArrayContains(FIELD_USERS, itm.getId())
                .get().addOnSuccessListener(queryDocumentSnapshots -> {

            if (queryDocumentSnapshots.isEmpty()) {
                return;
            }

            for (DocumentSnapshot item : queryDocumentSnapshots.getDocuments()) {
                listenersList.add(getChatMessagesListener(item));
            }
            size = listenersList.size() - 1;
        });
    }

    private ListenerRegistration getChatMessagesListener(DocumentSnapshot item) {
        return firestore.
                collection(COLLECTION_CONVERSATION).
                document(item.getId()).
                collection(COLLECTION_MESSAGES).
                addSnapshotListener((snapshot, e) -> {
                    if (e != null) {
                        Log.w(TAG, "listen:error", e);
                        return;
                    }

                    if (snapshot != null && !snapshot.isEmpty() && initialisation) {

                        for (DocumentChange message : snapshot.getDocumentChanges()) {
                            createNotificationForIncomeMessage(message);
                        }
                    }
                    setInitialisation();
                });
    }

    private void createNotificationForIncomeMessage(DocumentChange message) {
        if (message.getType() == DocumentChange.Type.ADDED) {

            MessageModel messageModel = message.getDocument().toObject(MessageModel.class);

            if (friendsDocumentIds.containsKey(messageModel.getSender())) {
                notificationHelper.createNotification(messageModel.getSender(), messageModel.getText(),
                        friendsDocumentIds.get(messageModel.getSender()), messageModel.getSender());
            }
            Log.d(TAG, "onEvent: " + messageModel.getSender());
        }
    }
}
