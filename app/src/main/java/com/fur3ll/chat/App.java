package com.fur3ll.chat;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;

import com.fur3ll.chat.di.Injection;
import com.fur3ll.chat.fabric.ViewModelFactory;
import com.fur3ll.chat.helper.NotificationHelper;
import com.google.firebase.firestore.FirebaseFirestore;

public class App extends Application {

    private ViewModelFactory viewModelFactory;

    private NotificationHelper notificationHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        notificationHelper = Injection.provideHelper(getApplicationContext(),
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));

        viewModelFactory = Injection.provideViewModelFactory(FirebaseFirestore.getInstance(),
                notificationHelper);
    }

    public ViewModelFactory getViewModelFactory() {
        return viewModelFactory;
    }

    public NotificationHelper getNotificationHelper() {
        return notificationHelper;
    }
}
