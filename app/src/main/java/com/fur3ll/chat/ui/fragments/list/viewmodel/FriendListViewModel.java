package com.fur3ll.chat.ui.fragments.list.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fur3ll.chat.helper.NotificationHelper;
import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.FriendModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;


public class FriendListViewModel extends ViewModel implements DatabaseNaming.CollectionUsers {

    private final String userEmail;
    private String userDocumentId;
    private String friendDocumentId;
    private CollectionReference userReference;
    private ListenerRegistration usersListener;
    private MutableLiveData<List<FriendModel>> mutableFriendsListLiveData = new MutableLiveData<>();
    private LiveData<List<FriendModel>> friendsListLiveData = mutableFriendsListLiveData;
    private MutableLiveData<List<String>> mutableDocumentId = new MutableLiveData<>();
    private LiveData<List<String>> documentIdLiveData = mutableDocumentId;
    private FirebaseFirestore firestore;
    private NotificationHelper notificationHelper;

    public FriendListViewModel(FirebaseFirestore firebaseFirestore, NotificationHelper notificationHelper) {
        this.firestore = firebaseFirestore;
        this.notificationHelper = notificationHelper;

        userEmail = FirebaseAuth.getInstance().getCurrentUser().getEmail();

        getFriends();

        initListener();
    }

    //getFriends from storage and add them to FriendModel list except current user,
    // create notification channel

    private void getFriends() {

        userReference = firestore.collection(COLLECTION_USERS);

        userReference
                .orderBy(FIELD_NAME, Query.Direction.DESCENDING)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {
            List<DocumentSnapshot> queryList = queryDocumentSnapshots.getDocuments();
            createNotificationChannels(queryList);
            List<FriendModel> list = getSortedFriendList(queryList);
            mutableFriendsListLiveData.postValue(list);
        });
    }

    private void initListener() {

        usersListener = userReference
                .orderBy(FIELD_NAME, Query.Direction.DESCENDING)
                .addSnapshotListener((snapshot, e) -> {
                    if (e != null) {
                        return;
                    }
                    if (snapshot != null && !snapshot.isEmpty()) {
                        mutableFriendsListLiveData.postValue(getSortedFriendList(snapshot.getDocuments()));
                    }
                });
    }

    public void findFriendByEmail(String userEmail) {
        userReference.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<DocumentSnapshot> list = task.getResult().getDocuments();
                List<String> useres = new ArrayList<>(2);

                for (DocumentSnapshot itm : list) {
                    if (itm.get(FIELD_EMAIL).equals(userEmail)) {
                        friendDocumentId = itm.getId();
                    } else if (itm.get(FIELD_EMAIL).equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {
                        userDocumentId = itm.getId();
                    }
                }
                useres.add(friendDocumentId);
                useres.add(userDocumentId);
                mutableDocumentId.setValue(useres);

                //clear liveData to prevents navigating to wrong chat fragment
                mutableDocumentId.postValue(new ArrayList<>(0));
            }
        });
    }

    private void createNotificationChannels(List<DocumentSnapshot> list) {

        for (DocumentSnapshot itm : list) {
            String channelName = itm.toObject(FriendModel.class).getName();
            String channelID = itm.getId();
            notificationHelper.createNotificationChannel(channelID, channelName);
        }
    }

    private List<FriendModel> getSortedFriendList(List<DocumentSnapshot> list) {
        List<FriendModel> friendList = new ArrayList<>(list.size());
        for (DocumentSnapshot itm : list) {
            if (!itm.toObject(FriendModel.class).getEmail().equals(userEmail)) {
                friendList.add(itm.toObject(FriendModel.class));
            }
        }
        return friendList;
    }

    public LiveData<List<FriendModel>> getFriendsListLiveData() {
        return friendsListLiveData;
    }

    public LiveData<List<String>> getDocumentIdLiveData() {
        return documentIdLiveData;
    }

    @Override
    protected void onCleared() {
        if (usersListener != null) {
            usersListener.remove();
        }
        super.onCleared();
    }
}
