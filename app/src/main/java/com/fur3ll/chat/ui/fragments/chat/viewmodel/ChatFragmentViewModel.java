package com.fur3ll.chat.ui.fragments.chat.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fur3ll.chat.mapper.BaseMapper;
import com.fur3ll.chat.mapper.MessageMapper;
import com.fur3ll.chat.models.ConversationsModel;
import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.FriendModel;
import com.fur3ll.chat.models.MessageItmType;
import com.fur3ll.chat.models.MessageModel;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class ChatFragmentViewModel extends ViewModel implements
        DatabaseNaming.CollectionConversation,
        DatabaseNaming.CollectionUsers,
        DatabaseNaming.CollectionConversation.CollectionMessages {

    private final String friendDocumentId;
    private final String userDocumentId;
    private String friendAvatar;
    private DocumentReference chatReference;
    private CollectionReference messageReference;
    private ListenerRegistration messageListener;
    private CollectionReference conversationCollection;
    private FirebaseFirestore firestore;
    private MutableLiveData<List<MessageModel>> mutableLiveData = new MutableLiveData<>();
    private LiveData<List<MessageModel>> listLiveData = mutableLiveData;

    public ChatFragmentViewModel(FirebaseFirestore firestore, String friendDocumentId, String userDocumentId) {
        this.firestore = firestore;
        this.friendDocumentId = friendDocumentId;
        this.userDocumentId = userDocumentId;
        initChatWithUser();
    }

    @Override
    protected void onCleared() {
        if (messageListener != null) {
            messageListener.remove();
        }
        super.onCleared();
    }

    public LiveData<List<MessageModel>> getListLiveData() {
        return listLiveData;
    }

    public void sendMessage(String messageText) {

        long time = Timestamp.now().getSeconds();

        BaseMapper<MessageModel, Map<String, Object>> mapper = new MessageMapper();

        messageReference.document(String.valueOf(time)).set(mapper.transformFrom(getMessage(messageText, time)));

    }

    private void initMessageListener(CollectionReference collectionReference) {
        messageListener = collectionReference.orderBy(FIELD_TIME, Query.Direction.ASCENDING).addSnapshotListener((snapshot, e) -> {

            if (e != null) {
                return;
            }

            if (snapshot != null && !snapshot.isEmpty()) {

                List<MessageModel> list = new ArrayList<>(snapshot.getDocuments().size());
                for (DocumentSnapshot itm : snapshot.getDocuments()) {
                    MessageModel message = itm.toObject(MessageModel.class);
                    if (message.getSender().equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())) {
                        message.setType(MessageItmType.TYPE_OUTBOX);
                    } else {
                        message.setType(MessageItmType.TYPE_INBOX);
                        message.setAvatar(friendAvatar);
                    }
                    list.add(message);
                }
                mutableLiveData.postValue(list);
            }
        });
    }

    private MessageModel getMessage(String text, Long time) {

        MessageModel messageModel = new MessageModel();
        messageModel.setSender(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        messageModel.setStatus(false);
        messageModel.setText(text);
        messageModel.setTime(time);
        return messageModel;
    }

    private void initChatWithUser() {
        if (friendDocumentId != null || !friendDocumentId.isEmpty()
                || userDocumentId != null || !userDocumentId.isEmpty()) {

            getFriendAvatar(friendDocumentId);

            conversationCollection = firestore.collection(COLLECTION_CONVERSATION);

            conversationCollection.whereArrayContains(FIELD_USERS, userDocumentId)
                    .get().addOnCompleteListener(task -> {

                        if (task.isSuccessful()) {

                            if (Objects.requireNonNull(task.getResult()).getDocuments().isEmpty()) {
                                //user don't have any chat, create chat with friend
                                createChatWithFriend();

                            } else {
                                //Search in user chats list if he have
                                // chat with friendDocumentId if not create
                                if (!findChatWithFriend(task)) {
                                    //chat with friend not found in list, create chat with friend
                                    createChatWithFriend();
                                }
                            }

                        }
                    });
        }

    }

    private void getFriendAvatar(String token) {
        firestore.collection(COLLECTION_USERS).document(token).
                get().addOnSuccessListener(documentSnapshot -> {
            FriendModel friendModel = documentSnapshot.toObject(FriendModel.class);

            if (friendModel.getAvatar() != null && !friendModel.getAvatar().isEmpty()) {
                friendAvatar = friendModel.getAvatar();
            }
        });
    }

    private boolean findChatWithFriend(Task<QuerySnapshot> task) {

        boolean found = false;

        for (DocumentSnapshot document : task.getResult().getDocuments()) {
            ConversationsModel conversation = document.toObject(ConversationsModel.class);
            if (conversation.getUsers().contains(friendDocumentId)) {
                //chat found, get messages init live data
                chatReference = document.getReference();
                messageReference = chatReference.collection(COLLECTION_MESSAGES);
                initMessageListener(messageReference);
                found = true;
            }
        }
        return found;
    }

    private void createChatWithFriend() {

        chatReference = firestore.collection(COLLECTION_CONVERSATION).document();
        chatReference.set(createConversation(usersChatList()))
                .addOnSuccessListener(aVoid -> {
                    messageReference = chatReference
                            .collection(COLLECTION_MESSAGES);
                    initMessageListener(messageReference);
                });
    }

    private List<String> usersChatList() {
        List<String> users = new ArrayList<>();
        users.add(friendDocumentId);
        users.add(userDocumentId);
        return users;
    }

    private ConversationsModel createConversation(List<String> users) {
        ConversationsModel conversationsModel = new ConversationsModel();
        conversationsModel.setName("hello");
        conversationsModel.setUsers(users);
        return conversationsModel;
    }
}
