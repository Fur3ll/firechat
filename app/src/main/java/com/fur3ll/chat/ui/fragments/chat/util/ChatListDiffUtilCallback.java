package com.fur3ll.chat.ui.fragments.chat.util;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.fur3ll.chat.models.MessageModel;

public class ChatListDiffUtilCallback extends DiffUtil.ItemCallback<MessageModel> {

    @Override
    public boolean areItemsTheSame(@NonNull MessageModel oldItem, @NonNull MessageModel newItem) {
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areContentsTheSame(@NonNull MessageModel oldItem, @NonNull MessageModel newItem) {
        return oldItem.equals(newItem);
    }
}
