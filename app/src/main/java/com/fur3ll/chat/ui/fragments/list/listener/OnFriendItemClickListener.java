package com.fur3ll.chat.ui.fragments.list.listener;

@FunctionalInterface
public interface OnFriendItemClickListener {
    void onClick(int position);
}
