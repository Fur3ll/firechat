package com.fur3ll.chat.ui.fragments.list;

import android.app.SearchManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fur3ll.chat.App;
import com.fur3ll.chat.R;
import com.fur3ll.chat.models.FriendModel;
import com.fur3ll.chat.ui.fragments.list.adapter.FriendListAdapter;
import com.fur3ll.chat.ui.fragments.list.util.FriendListDiffUtilCallback;
import com.fur3ll.chat.ui.fragments.list.viewmodel.FriendListViewModel;

import java.util.ArrayList;
import java.util.List;


public class FriendListFragment extends Fragment {

    private FriendListAdapter friendListAdapter;
    private FriendListViewModel friendListViewModel;
    private List<FriendModel> list = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        friendListAdapter.submitList(list);
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate
                (R.layout.fragment_friend_list, container, false);

        initRecycler(view);

        initViewModel();

        initObservers();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.friends_search, menu);

        SearchManager searchManager =
                (SearchManager) requireActivity().getSystemService(getContext().SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.isEmpty()) {
                    friendListAdapter.submitList(list);
                } else {
                    List<FriendModel> filteredList = new ArrayList<>(list.size());
                    for (FriendModel itm : list) {
                        if (itm.getName().toLowerCase().contains(s.toLowerCase())) {
                            filteredList.add(itm);
                        }
                    }
                    friendListAdapter.submitList(filteredList);
                }
                return false;
            }
        });
    }

    private void initViewModel() {

        friendListViewModel = new ViewModelProvider(this,
                ((App) requireActivity().getApplication()).getViewModelFactory()).
                get(FriendListViewModel.class);
    }

    private void initRecycler(final View view) {

        RecyclerView recyclerView = view.findViewById(R.id.friends_list_rv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);

        FriendListDiffUtilCallback differ = new FriendListDiffUtilCallback();

        friendListAdapter = new FriendListAdapter(differ, position -> {
            friendListViewModel.findFriendByEmail(list.get(position).getEmail());
            friendListViewModel.getDocumentIdLiveData().observe(getViewLifecycleOwner(), documentsId -> {
                if (documentsId.size() == 2) {
                    FriendListFragmentDirections.ActionFriendListFragmentToChatFragment action =
                            FriendListFragmentDirections
                                    .actionFriendListFragmentToChatFragment(documentsId.get(0), documentsId.get(1));
                    Navigation.findNavController(getView()).navigate(action);
                }
            });
        });

        recyclerView.setAdapter(friendListAdapter);
    }

    private void initObservers() {
        friendListViewModel.getFriendsListLiveData().observe(getViewLifecycleOwner(), friendModels -> {
            list = friendModels;
            friendListAdapter.submitList(list);
        });
    }
}
