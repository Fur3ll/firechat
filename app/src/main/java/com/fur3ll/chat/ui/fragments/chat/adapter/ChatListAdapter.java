package com.fur3ll.chat.ui.fragments.chat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.fur3ll.chat.R;
import com.fur3ll.chat.models.MessageItmType;
import com.fur3ll.chat.models.MessageModel;
import com.squareup.picasso.Picasso;

public class ChatListAdapter extends ListAdapter<MessageModel, RecyclerView.ViewHolder> {

    private final String DEFAULT_AVATAR = "https://images-na.ssl-images-amazon.com/images/I/41WpCPr9DOL._SX425_.jpg";
    public ChatListAdapter(@NonNull DiffUtil.ItemCallback<MessageModel> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                      @MessageItmType.MessageListItem int viewType) {

        int layout;

        RecyclerView.ViewHolder viewHolder;

        switch (viewType) {
            case MessageItmType.TYPE_INBOX:
                View inboxView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.chat_list_item_inbox,parent,false);
                viewHolder = new InboxMessageViewHolder(inboxView);
                break;
            case MessageItmType.TYPE_OUTBOX:
                View outboxView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.chat_list_item_outbox,parent,false);
                viewHolder = new OutboxMessageViewHolder(outboxView);
                break;
            default:
                viewHolder = null;
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {

        @MessageItmType.MessageListItem
        int type = getItem(position).getType();
        switch (type) {
            case MessageItmType.TYPE_INBOX:
                return MessageItmType.TYPE_INBOX;
            case MessageItmType.TYPE_OUTBOX:
                return MessageItmType.TYPE_OUTBOX;
        }
        return -1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageModel item = getItem(position);

        @MessageItmType.MessageListItem
                int viewType = holder.getItemViewType();
        switch (viewType) {
            case MessageItmType.TYPE_INBOX:
                ((ChatListAdapter.InboxMessageViewHolder) holder).bind(item);
                break;
            case MessageItmType.TYPE_OUTBOX:
                ((OutboxMessageViewHolder) holder).bind(item);
                break;
        }
    }

    public class OutboxMessageViewHolder extends RecyclerView.ViewHolder {

        TextView textMessage;

        public OutboxMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            textMessage = itemView.findViewById(R.id.message_text);
        }

        public void bind(@NonNull final MessageModel item) {
            textMessage.setText(item.getText());
        }
    }

    public class InboxMessageViewHolder extends RecyclerView.ViewHolder {

        ImageView viewAvatar;
        TextView textTitle;
        TextView textMessage;

        public InboxMessageViewHolder(@NonNull View itemView) {
            super(itemView);
            viewAvatar = itemView.findViewById(R.id.avatar_v);
            textTitle = itemView.findViewById(R.id.title_tv);
            textMessage = itemView.findViewById(R.id.message_tv);
        }

        public void bind(@NonNull final MessageModel item) {

            String name = item.getSender();
            String text = item.getText();
            String avatar = item.getAvatar();

            if (name != null || !name.isEmpty()) {
                textTitle.setText(name);
            }

            if (text != null || !text.isEmpty()) {
                textMessage.setText(text);
            }

            if (avatar == null || avatar.isEmpty()) {
                avatar = DEFAULT_AVATAR;
            }

            Picasso.get()
                    .load(avatar)
                    .resizeDimen(R.dimen.friend_list_item_avatar_dimens, R.dimen.friend_list_item_avatar_dimens)
                    .centerCrop()
                    .into(viewAvatar);
        }
    }
}
