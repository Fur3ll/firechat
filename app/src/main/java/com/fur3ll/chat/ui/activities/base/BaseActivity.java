package com.fur3ll.chat.ui.activities.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.fur3ll.chat.services.MessageListenerService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public abstract class BaseActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentUser = getAuth().getCurrentUser();
    }

    public FirebaseAuth getAuth() {
        return auth;
    }

    public FirebaseUser getCurrentUser() {
        return currentUser;
    }

    public FirebaseFirestore getFirestore() {
        return firestore;
    }

    protected void createMessageListenerService() {
        Intent intent = new Intent(this, MessageListenerService.class);
        intent.setAction(MessageListenerService.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);
    }
}
