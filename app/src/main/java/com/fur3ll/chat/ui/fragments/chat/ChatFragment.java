package com.fur3ll.chat.ui.fragments.chat;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fur3ll.chat.App;
import com.fur3ll.chat.R;
import com.fur3ll.chat.fabric.ViewModelFactory;
import com.fur3ll.chat.models.MessageModel;
import com.fur3ll.chat.ui.fragments.chat.adapter.ChatListAdapter;
import com.fur3ll.chat.ui.fragments.chat.util.ChatListDiffUtilCallback;
import com.fur3ll.chat.ui.fragments.chat.viewmodel.ChatFragmentViewModel;

import java.util.List;

public class ChatFragment extends Fragment {

    private ChatFragmentViewModel chatFragmentViewModel;
    private ChatListAdapter chatListAdapter;
    private List<MessageModel> list;
    private String friendDocumentId;
    private String userDocumentId;
    private ImageButton sendIb;
    private EditText messageEt;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate
                (R.layout.fragment_chat, container, false);

        getSafeArgs();
        initViewModel();
        initViewsAndListeners(view);
        initRecycler(view);
        initObserver();

        return view;
    }

    private void getSafeArgs() {
        friendDocumentId = ChatFragmentArgs.fromBundle(getArguments()).getFriendDocumentId();
        userDocumentId = ChatFragmentArgs.fromBundle(getArguments()).getUserDocumentId();
    }

    private void initViewsAndListeners(View view) {
        sendIb = view.findViewById(R.id.send_ib);
        messageEt = view.findViewById(R.id.message_et);

        initInputListener(messageEt);

        initSendButtonListener(sendIb);
    }

    private void initViewModel() {

        ViewModelFactory factory = ((App) requireActivity().getApplication()).getViewModelFactory();
        factory.setUserDocumentId(userDocumentId);
        factory.setFriendDocumentId(friendDocumentId);
        chatFragmentViewModel = new ViewModelProvider(this, factory).
                get(ChatFragmentViewModel.class);
    }

    private void initRecycler(View view) {

        recyclerView = view.findViewById(R.id.chat_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        ChatListDiffUtilCallback chatListDiffUtilCallback = new ChatListDiffUtilCallback();
        chatListAdapter = new ChatListAdapter(chatListDiffUtilCallback);
        recyclerView.setAdapter(chatListAdapter);
        initListenerSoftInputScroll(recyclerView);
    }

    private void initInputListener(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    if (!charSequence.toString().trim().isEmpty()) {
                        sendIb.setVisibility(View.VISIBLE);
                    }
                } else {
                    sendIb.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void initSendButtonListener(View imageButton) {
        imageButton.setOnClickListener(view -> {
            chatFragmentViewModel.sendMessage(messageEt.getText().toString());
            messageEt.getText().clear();
        });
    }

    private void initObserver() {
        chatFragmentViewModel.getListLiveData().observe(getViewLifecycleOwner(), messageModels -> {
            list = messageModels;
            chatListAdapter.submitList(list);
            scrollRecyclerToBottom(recyclerView);
        });
    }

    private void scrollRecyclerToBottom(RecyclerView recyclerView) {
        recyclerView.postDelayed(() -> recyclerView.smoothScrollToPosition(chatListAdapter.getItemCount()), 100);
    }

    //scroll rv to bottom when keyboard is open/close
    private void initListenerSoftInputScroll(RecyclerView recyclerView) {
        recyclerView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                scrollRecyclerToBottom(recyclerView);
            }
        });
    }
}
