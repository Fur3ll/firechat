package com.fur3ll.chat.ui.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.fur3ll.chat.R;
import com.fur3ll.chat.ui.activities.home.HomeActivity;
import com.fur3ll.chat.ui.activities.base.BaseActivity;
import com.fur3ll.chat.ui.activities.login.SignInActivity;
import com.google.firebase.auth.FirebaseUser;

public class SplashActivity extends BaseActivity {

    private final int SPLASH_DISPLAY_DELAY = 1000;
    private final Handler mHandler = new Handler();
    private final Launcher mLauncher = new Launcher();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mHandler.postDelayed(mLauncher, SPLASH_DISPLAY_DELAY);
    }

    private void checkUser(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(this, HomeActivity.class));
        } else {
            startActivity(new Intent(this, SignInActivity.class));
        }
    }

    private void launch() {
        if (!isFinishing()) {
            checkUser(getCurrentUser());
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }

    private class Launcher implements Runnable {
        @Override
        public void run() {
            launch();
        }
    }

    @Override
    protected void onStop() {
        mHandler.removeCallbacksAndMessages(null);
        super.onStop();
    }
}
