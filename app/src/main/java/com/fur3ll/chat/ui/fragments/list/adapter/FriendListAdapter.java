package com.fur3ll.chat.ui.fragments.list.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.fur3ll.chat.R;
import com.fur3ll.chat.models.FriendModel;
import com.fur3ll.chat.ui.fragments.list.listener.OnFriendItemClickListener;
import com.squareup.picasso.Picasso;

public class FriendListAdapter extends ListAdapter<FriendModel, FriendListAdapter.ItemViewHolder> {

    private final String DEFAULT_AVATAR = "https://images-na.ssl-images-amazon.com/images/I/41WpCPr9DOL._SX425_.jpg";
    private OnFriendItemClickListener onFriendItemClickListener;

    public FriendListAdapter(@NonNull DiffUtil.ItemCallback<FriendModel> diffCallback,
                             OnFriendItemClickListener onFriendItemClickListener) {
        super(diffCallback);
        this.onFriendItemClickListener = onFriendItemClickListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.friend_list_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        view.setOnClickListener(view1 -> {
            int position = itemViewHolder.getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                onFriendItemClickListener.onClick(position);
            }
        });
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        FriendModel item = getItem(position);

        String itemAvatar = item.getAvatar();

        if (itemAvatar == null || itemAvatar.isEmpty()) {
            itemAvatar = DEFAULT_AVATAR;
        }

        Picasso.get()
                .load(itemAvatar)
                .resizeDimen(R.dimen.friend_list_item_avatar_dimens, R.dimen.friend_list_item_avatar_dimens)
                .centerCrop()
                .into(holder.imageView);


        holder.bind(item);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textName;
        TextView textEmail;
        ImageView imageView;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.name_tv);
            textEmail = itemView.findViewById(R.id.email_tv);
            imageView = itemView.findViewById(R.id.avatar_iv);
        }

        public void bind(@NonNull final FriendModel item) {
            textName.setText(item.getName());
            textEmail.setText(item.getEmail());
        }
    }
}
