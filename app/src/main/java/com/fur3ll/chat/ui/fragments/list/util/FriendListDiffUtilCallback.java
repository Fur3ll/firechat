package com.fur3ll.chat.ui.fragments.list.util;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.fur3ll.chat.models.FriendModel;

public class FriendListDiffUtilCallback extends DiffUtil.ItemCallback<FriendModel> {

    @Override
    public boolean areItemsTheSame(@NonNull FriendModel oldItem, @NonNull FriendModel newItem) {
        return oldItem.equals(newItem);
    }

    @Override
    public boolean areContentsTheSame(@NonNull FriendModel oldItem, @NonNull FriendModel newItem) {
        return oldItem.equals(newItem);
    }
}
