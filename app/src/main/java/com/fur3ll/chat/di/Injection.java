package com.fur3ll.chat.di;

import android.app.NotificationManager;
import android.content.Context;

import com.fur3ll.chat.fabric.ViewModelFactory;
import com.fur3ll.chat.helper.NotificationHelper;
import com.google.firebase.firestore.FirebaseFirestore;

public class Injection {

    public static NotificationHelper provideHelper(Context context, NotificationManager notificationManager) {
        return new NotificationHelper(context, notificationManager);
    }

    public static ViewModelFactory provideViewModelFactory(FirebaseFirestore firebaseFirestore, NotificationHelper notificationHelper) {
        return new ViewModelFactory(firebaseFirestore, notificationHelper);
    }
}
