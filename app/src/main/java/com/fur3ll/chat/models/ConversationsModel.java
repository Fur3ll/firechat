package com.fur3ll.chat.models;

import java.util.List;
import java.util.Objects;

public class ConversationsModel {

    private String name;

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    private List<String> users;

    public ConversationsModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConversationsModel that = (ConversationsModel) o;
        return Objects.equals(name, that.name) &&
                users.equals(that.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, users);
    }
}
