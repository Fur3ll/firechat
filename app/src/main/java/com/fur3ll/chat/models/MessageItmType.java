package com.fur3ll.chat.models;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class MessageItmType {

    public final static int TYPE_INBOX = 1;
    public final static int TYPE_OUTBOX = 2;

    @IntDef ({TYPE_INBOX, TYPE_OUTBOX})

    @Retention(RetentionPolicy.SOURCE)

    public @interface MessageListItem{}

    @MessageListItem
    private int viewType;

    @MessageListItem
    public int getViewType() {
        return viewType;
    }

    public void setViewType(@MessageListItem int viewType) {
        this.viewType = viewType;
    }
}
