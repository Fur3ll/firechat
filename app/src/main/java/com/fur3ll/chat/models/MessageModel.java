package com.fur3ll.chat.models;


import java.util.Objects;

public class MessageModel {

    private String sender;
    private boolean status;
    private String text;
    private long time;
    private String avatar;

    public MessageModel() {
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @MessageItmType.MessageListItem
    private int type;

    public int getType() {
        return type;
    }

    public void setType(@MessageItmType.MessageListItem int type) {
        this.type = type;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageModel that = (MessageModel) o;
        return isStatus() == that.isStatus() &&
                getTime() == that.getTime() &&
                getSender().equals(that.getSender()) &&
                getText().equals(that.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSender(), isStatus(), getText(), getTime());
    }
}
