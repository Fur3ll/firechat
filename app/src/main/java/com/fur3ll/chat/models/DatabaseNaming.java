package com.fur3ll.chat.models;

public interface DatabaseNaming {
    interface CollectionUsers {
        String COLLECTION_USERS = "users";
        String FIELD_TOKEN = "token";
        String FIELD_EMAIL = "email";
        String FIELD_AVATAR = "avatar";
        String FIELD_NAME = "name";
    }
    interface CollectionConversation {
        String COLLECTION_CONVERSATION = "conversation";
        String FIELD_NAME = "name";
        String FIELD_USERS = "users";

        interface CollectionMessages {
            String COLLECTION_MESSAGES = "messages";
            String FIELD_SENDER = "sender";
            String FIELD_STATUS = "status";
            String FIELD_TEXT = "text";
            String FIELD_TIME = "time";
        }
    }
}
