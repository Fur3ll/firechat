package com.fur3ll.chat.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

public abstract class BaseMapper<E, C> {

    public abstract C transformFrom(@Nullable E from);

    public List<C> transformAllFrom(@Nullable List<E> list) {
        if(list == null) return Collections.emptyList();

        final List<C> newList = new ArrayList<>(list.size());

        for (int i = 0; i < list.size(); i++) {
            newList.add(transformFrom(list.get(i)));
        }
        return newList;
    }
}
