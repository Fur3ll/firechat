package com.fur3ll.chat.mapper;

import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.MessageModel;

import java.util.HashMap;
import java.util.Map;

public class MessageMapper extends BaseMapper<MessageModel, Map<String, Object>> implements DatabaseNaming.CollectionConversation.CollectionMessages {

    @Override
    public Map<String, Object> transformFrom(MessageModel model) {

        Map<String, Object> message = new HashMap<>();
        message.put(FIELD_SENDER, model.getSender());
        message.put(FIELD_TEXT, model.getText());
        message.put(FIELD_TIME, model.getTime());
        message.put(FIELD_STATUS, model.isStatus());

        return message;
    }
}
