package com.fur3ll.chat.mapper;

import com.fur3ll.chat.models.DatabaseNaming;
import com.fur3ll.chat.models.FriendModel;

import java.util.HashMap;
import java.util.Map;

public class FriendMapper extends BaseMapper<FriendModel, Map<String, String>> implements DatabaseNaming.CollectionUsers {

    @Override
    public Map<String, String> transformFrom(FriendModel model) {

        Map<String, String> friend = new HashMap<>();
        friend.put(FIELD_NAME, model.getName());
        friend.put(FIELD_AVATAR, model.getAvatar());
        friend.put(FIELD_TOKEN, model.getToken());
        friend.put(FIELD_EMAIL, model.getEmail());

        return friend;
    }
}
